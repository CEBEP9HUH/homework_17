﻿#include <iostream>

class Vector
{
public:
    Vector() : x(3), y (4), z(5)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << "x: " << x << "\ny: " << y << "\nz: " << z << std::endl;
    }
    double Length()
    {
        return sqrt(x * x + y * y + z * z);
    }
private:
    double x = 0;
    double y = 0;
    double z = 0;
};


int main()
{
    Vector v;
    v.Show();
    std::cout << "Vector's length equals " << v.Length() << std::endl;
}
